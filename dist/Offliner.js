(function() {
  var Offliner;

  Offliner = {
    ajax: {
      method: "HEAD"
    },
    url: "/ping",
    state: null,
    forceUp: false,
    on: function(state, fn) {
      return Offliner._states[state].push(fn);
    },
    check: function(online, offline) {
      if (this.forceUp) {
        Offliner._updateStateTo('up', online);
        return;
      }
      return $.ajax({
        type: Offliner.ajax.method,
        url: Offliner.url,
        success: function() {
          return Offliner._updateStateTo('up', online);
        },
        error: function() {
          return Offliner._updateStateTo('down', offline || online);
        }
      });
    },
    _updateStateTo: function(after, fn) {
      var state, _i, _len, _ref;
      if (Offliner.state !== after) {
        Offliner.state = after;
        _ref = Offliner._states[after];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          state = _ref[_i];
          state();
        }
      }
      return typeof fn === "function" ? fn(after) : void 0;
    },
    _states: {
      down: [],
      up: []
    }
  };

  (typeof exports !== "undefined" && exports !== null ? exports : window).Offliner = Offliner;

}).call(this);
