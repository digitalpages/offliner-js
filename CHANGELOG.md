<a name="1.3.0"></a>
## 1.3.0 (2014-12-22)


#### Features

* **callbacks:** add forceUp option, always online if forceUp is true ((4efb93ea))


<a name="1.2.1"></a>
### 1.2.1 (2014-12-16)

