GLOBAL.document = require('jsdom').jsdom()
GLOBAL.window = document.parentWindow
GLOBAL.$ = require("jquery")
offliner = require('../src/offliner.js').Offliner
should = require 'should'
sinon = require 'sinon'

stub = null

describe 'Offliner', ->
  describe '.check', ->
    it  'reads .ajax.method', ->
      offliner.ajax.method = "GET"
      sinon.spy $, "ajax"
      offliner.check()
      $.ajax.getCall(0).args[0].should.have.property('type', "GET")

    afterEach ->
      $.ajax.restore()
      offliner.ajax.method = "HEAD"

  describe '.check', ->
    beforeEach ->
      stub = sinon.stub $, "ajax"
      offliner._states = down: [], up: []

    describe 'when online', ->
      beforeEach ->
        stub.yieldsTo "success"

      it 'executes the first callback', (done) ->
        offliner.check -> done()

      it 'passes state to callback', (done) ->
        offliner.check (state) -> done() if state is 'up'

      describe 'when state null', ->
        beforeEach ->
          offliner.state = null

        it 'triggers .on("up")', (done) ->
          offliner.on 'up', done
          offliner.check()

        it 'changes the state to "up"', (done) ->
          offliner.check ->
            offliner.state.should.equal('up')
            done()

      describe 'when state down', ->
        beforeEach ->
          offliner.state = 'down'

        it 'triggers .on("up")', (done) ->
          offliner.on 'up', done
          offliner.check()

        it 'changes the state to "up"', (done) ->
          offliner.check ->
            offliner.state.should.equal('up')
            done()

      describe 'when state up', ->
        beforeEach ->
          offliner.state = 'up'

        it 'doesnt trigger .on("up")', (done) ->
          offliner.on 'up', -> new Error()
          offliner.check()
          setTimeout done, 50

    describe 'when offline', ->
      beforeEach ->
        stub.yieldsTo "error"

      it "always execute the first callback if it's the only one", (done) ->
        offliner.check -> done()

      it 'executes the second callback', (done) ->
        offliner.check (->), -> done()

      it 'passes state to callback', (done) ->
        offliner.check (state) -> done() if state is 'down'

      describe 'when state up', ->
        beforeEach ->
          offliner.state = 'up'

        it 'triggers .on("down")', (done) ->
          offliner.on 'down', done
          offliner.check()

        it 'changes the state to "down"', (done) ->
          offliner.check (->), ->
            offliner.state.should.equal('down')
            done()


      describe 'when state down', ->
        beforeEach ->
          offliner.state = 'down'

        it 'doesnt trigger .on("down")', (done) ->
          offliner.on 'down', -> new Error()
          offliner.check()
          setTimeout done, 50

      describe 'with forceUp equal true',(done) ->
        beforeEach ->
          offliner.state = 'down'
          offliner.forceUp = true

        it 'triggers .on("up")', (done) ->
          offliner.on 'up', done
          offliner.check()

        it 'changes the state to up ', (done) ->
          offliner.state = 'down'
          offliner.check (state)->
            offliner.state.should.equal('up')
            state.should.equal('up')
            done()

    afterEach ->
      stub.restore()
