Offliner =
  ajax: method: "HEAD"
  url: "/ping"
  state: null
  forceUp: false

  on: (state, fn) ->
    Offliner._states[state].push fn

  check: (online, offline) ->
    if @forceUp
      Offliner._updateStateTo('up', online)
      return

    $.ajax
      type: Offliner.ajax.method
      url: Offliner.url
      success: -> Offliner._updateStateTo 'up', online
      error: -> Offliner._updateStateTo 'down', offline || online

  _updateStateTo: (after, fn) ->
    if Offliner.state isnt after
      Offliner.state = after
      for state in Offliner._states[after]
        state()
    fn?(after)

  _states: down: [], up: []

(exports ? window).Offliner = Offliner
